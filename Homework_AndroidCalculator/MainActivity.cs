﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using System.Collections.Generic;

namespace Homework_AndroidCalculator
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        //Button but1, but2, but3, but4, but5, but6, but7, but8, but9, but0;
        //string Display;
        //List<float> buffer;
        float a, b;
        char operand;
        bool operatonFinished = false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            Init();
        }
        private void Init()
        {
            EditText editText = FindViewById<EditText>(Resource.Id.editText1);
            Button but0 = FindViewById<Button>(Resource.Id.but0);
            but0.Click += (sender, e) =>
            {
                editText.Text = editText.Text + but0.Text;
            };
            Button but1 = FindViewById<Button>(Resource.Id.but1);
            but1.Click += (sender, e) => {
                editText.Text = editText.Text + but1.Text;
            };
            Button but2 = FindViewById<Button>(Resource.Id.but2);
            but2.Click += (sender, e) => {
                editText.Text = editText.Text + but2.Text;
            };
            Button but3 = FindViewById<Button>(Resource.Id.but3);
            but3.Click += (sender, e) => {
                editText.Text = editText.Text + but3.Text;
            };
            Button but4 = FindViewById<Button>(Resource.Id.but4);
            but4.Click += (sender, e) => {
                editText.Text = editText.Text + but4.Text;
            };
            Button but5 = FindViewById<Button>(Resource.Id.but5);
            but5.Click += (sender, e) => {
                editText.Text = editText.Text + but5.Text;
            };
            Button but6 = FindViewById<Button>(Resource.Id.but6);
            but6.Click += (sender, e) => {
                editText.Text = editText.Text + but6.Text;
            };
            Button but7 = FindViewById<Button>(Resource.Id.but7);
            but7.Click += (sender, e) => {
                editText.Text = editText.Text + but7.Text;
            };
            Button but8 = FindViewById<Button>(Resource.Id.but8);
            but8.Click += (sender, e) => {
                editText.Text = editText.Text + but8.Text;
            };
            Button but9 = FindViewById<Button>(Resource.Id.but9);
            but9.Click += (sender, e) => {
                editText.Text = editText.Text + but9.Text;
            };
            Button butPoint = FindViewById<Button>(Resource.Id.butPoint);
            butPoint.Click += (sender, e) => {
                editText.Text = editText.Text + butPoint.Text;
            };
            Button butApp = FindViewById<Button>(Resource.Id.butApp);
            butApp.Click += (sender, e) => {
                //DoStuff(editText);
                operand = '+';
                float.TryParse(editText.Text, out a);
                editText.Text = editText.Text + " + ";
            };
            Button butMul = FindViewById<Button>(Resource.Id.butMul);
            butMul.Click += (sender, e) => {
                //DoStuff(editText);
                operand = 'x';
                float.TryParse(editText.Text, out a);
                editText.Text = editText.Text + " x ";
            };
            Button butMin = FindViewById<Button>(Resource.Id.butMin);
            butMin.Click += (sender, e) => {
                //DoStuff(editText);
                operand = '-';
                float.TryParse(editText.Text, out a);
                editText.Text = editText.Text + " - ";
            };
            Button butDiv = FindViewById<Button>(Resource.Id.butDiv);
            butDiv.Click += (sender, e) => {
                operand = '/';
                float.TryParse(editText.Text, out a);
                editText.Text = editText.Text + " / ";
            };

            Button butEquals = FindViewById<Button>(Resource.Id.butEquals);
            butEquals.Click += (sender, e) =>
            {
                DoStuff(editText);
            };

            Button butC = FindViewById<Button>(Resource.Id.butC);
            butC.Click += (sender, e) =>
            {
                editText.Text = string.Empty;
                operand = 'e';
            };

            Button butCE = FindViewById<Button>(Resource.Id.butCE);
            butCE.Click += (sender, e) =>
            {
                if (operand.Equals('e'))
                {
                    editText.Text = string.Empty;
                }
                else
                {
                    editText.Text = a.ToString() + " " + operand + " ";
                }
                operand = 'e';
            };

            Button butSign = FindViewById<Button>(Resource.Id.butSign);
            butSign.Click += (sender, e) =>
            {
                if (operand.Equals('e'))
                {
                    float.TryParse(editText.Text, out a);
                    a = a * (-1);
                    editText.Text = a.ToString();
                }
                else
                {
                    float.TryParse(editText.Text.Split(new string[] { "+", "-", "x", "/" }, System.StringSplitOptions.RemoveEmptyEntries)[1], out b);
                    b = b * (-1);
                    editText.Text = a.ToString() + " " + operand + " " + b.ToString();
                }
            };

        }

        private void DoStuff(EditText editText)
        {
            if (!string.IsNullOrWhiteSpace(editText.Text))
            {
                if(b==0)
                    float.TryParse(editText.Text.Split(new string[] { "+", "-", "x", "/" }, System.StringSplitOptions.RemoveEmptyEntries)[1], out b);
                if (!operand.Equals('e'))
                {
                    switch (operand)
                    {
                        case '+': a = a + b; break;
                        case '-': a = a - b; break;
                        case 'x': a = a * b; break;
                        case '/': a = a / b; break;
                    }
                    b = 0;
                    editText.Text = a.ToString();
                    operand = 'e';

                }
                else
                    editText.Text = editText.Text;

                operatonFinished = true;
            }
            else
                editText.Text = editText.Text;
        }
    }
}

